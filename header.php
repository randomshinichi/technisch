<?php wp_get_archives('type=monthly&format=link'); ?>
<?php //comments_popup_script(); // off by default ?>
<?php wp_head(); ?>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" /> 
<meta name="google-site-verification" content="iWFt6LOb8k36iOic54Ab1IeJOiLMMKnA-HSu2wnNSrs" />
</head>
<body>
  <div class="wrtotop"></div>
  <div id="header">
    <div id="headerimage">
      <div id="logos">
        <div id="headerlogoD"><a class="aheaderlogo" href="http://dasaku.net"></a></div>
        <div id="headerlogoRR"><a class="aheaderlogo" href="<?php bloginfo('url');?>"></a></div>
      </div>
    </div>
  </div>
  <div class="wrtobottom">
    <div id="topmenu">
    <ul>
      <li onmouseover="this.className='shamelessripfromrandomc'" onmouseout="this.className=''">
        <div id="categories"></div>
        <ul><?php wp_list_categories('title_li='); ?></ul>
      </li>
      <li onmouseover="this.className='shamelessripfromrandomc'" onmouseout="this.className=''">
        <div id="pages"></div>
        <ul><?php wp_list_pages('title_li='); ?></ul>
      </li>
      <li onmouseover="this.className='shamelessripfromrandomc'" onmouseout="this.className=''">
        <div id="archives"></div>
        <ul><?php wp_get_archives('type=monthly'); ?></ul>
      </li>
      <li onmouseover="this.className='shamelessripfromrandomc'" onmouseout="this.className=''">
        <div id="links"></div>
        <ul><?php wp_list_bookmarks('title_li=&categorize=0'); ?></ul>
      </li>
      <li onmouseover="this.className='shamelessripfromrandomc'" onmouseout="this.className=''">
        <div id="meta"></div>
        <ul>
          <?php wp_register(); ?>
          <li><?php wp_loginout(); ?></li>
          <li><a href="http://validator.w3.org/check/referer" title="<?php _e('This page validates as XHTML 1.0 Transitional'); ?>"><?php _e('Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr>'); ?></a></li>
          <?php wp_meta(); ?>
        </ul>
      </li>
      <li>
        <?php include(TEMPLATEPATH.'/searchform.php'); ?>
      </li>
    </ul>
    </div>
  </div>
