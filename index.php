﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<script type="text/javascript">
<!--
window.onload=show;
function show(id) {
var d = document.getElementById(id);
	for (var i = 1; i<=10; i++) {
		if (document.getElementById('smenu'+i)) {document.getElementById('smenu'+i).style.display='none';}
	}
if (d) {d.style.display='block';}
}
//-->
</script>
<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />	
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats please -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php get_header(); ?>
  <div class="widgettr">
    <div class="widgettop"></div>
      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Top Sidebar') ) : ?><?php include(TEMPLATEPATH.'/functions.php'); ?><?php endif; ?>
      <div class="clear"></div>
    <div class="widgetbottom"></div>
  </div>
  <div id="container"><div id="containertop"></div>
    <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
    <div class="post">
      <div class="postmetadata"><?php the_date('j F Y') ?> <?php the_category(', ') ?></div>
      <div class="permalink"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div><?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?> <?php edit_post_link('Edit', '   ', ''); ?>
      <div class="entry"><?php the_content() ?></div>
	  <?php the_meta(); ?>
    </div>
    <?php endwhile; ?>
    <div class="navigation"><?php posts_nav_link();?></div>
    <?php else : ?>
    <?php endif; ?>
    <div id="containerbottom"></div>
  </div>
  <div class="widgettr">
    <div class="widgettop"></div>
      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Bottom Sidebar') ) : ?><?php include(TEMPLATEPATH.'/functions.php'); ?><?php endif; ?>
      <div class="clear"></div>
    <div class="widgetbottom"></div>
  </div>
  <?php get_footer(); ?>
</body>
</html>